import model.MonitoredData;
import org.joda.time.Duration;
import org.joda.time.LocalDate;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;
import parser.Parser;

import java.io.IOException;
import java.util.Map;

public class MainClass {
    public static void main(String[] args) {

        PeriodFormatter pretty = new PeriodFormatterBuilder()
                .appendDays()
                .appendSuffix(" day, ", " days, ")
                .appendHours()
                .appendSuffix(" hour, ", " hours, ")
                .appendMinutes()
                .appendSuffix(" minute, ", " minutes, ")
                .appendSeconds()
                .appendSuffix(" second ", " seconds ")
                .toFormatter();

        System.out.println("Subject I");
        Parser P = new Parser();
        try {
            P.parse();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(P.getDataList());

        System.out.println("\nSubject II");
        System.out.println(P.getDayCount());

        System.out.println("\nSubject III");
        System.out.println(P.getActivitiesCount().entrySet());

        System.out.println("\nSubject IV");
        for(Map.Entry<LocalDate, Map<String, Long>> M : P.getActivitiesPerDay().entrySet()) {
            System.out.println(M);
        }

        System.out.println("\nSubject V");
        for(Map.Entry<MonitoredData, Duration> M : P.getDurationPerEntry().entrySet()) {
            System.out.println(M.getKey() + " " + pretty.print(M.getValue().toPeriod()));
        }

        System.out.println("\nSubject VI");
        for(Map.Entry<String, Duration> M : P.getDurationPerActivity().entrySet()) {
            System.out.println(M.getKey() + " " + pretty.print(M.getValue().toPeriod()));
        }

        System.out.println("\nSubject VII");
        System.out.println(P.getFilteredActivities());
    }
}
