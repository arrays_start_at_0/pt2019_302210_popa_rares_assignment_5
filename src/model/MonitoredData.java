package model;

import org.joda.time.DateTimeZone;
import org.joda.time.Duration;
import org.joda.time.LocalDateTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class MonitoredData {
    private LocalDateTime start_time;
    private LocalDateTime end_time;
    private String activity;

    public MonitoredData(String start, String end, String activity) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            start_time = new LocalDateTime(format.parse(start));
            end_time = new LocalDateTime(format.parse(end));
            this.activity = activity.replaceAll("\\s", "");
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public LocalDateTime getStart() { return start_time; }
    public String getActivity() { return activity; }
    public Duration calcDuration() { return new Duration(start_time.toDateTime(DateTimeZone.UTC), end_time.toDateTime(DateTimeZone.UTC));}

    @Override
    public String toString() {
        return "MonitoredData{" +
                "start_time=" + start_time +
                ", end_time=" + end_time +
                ", activity='" + activity + '\'' +
                '}';
    }
}
