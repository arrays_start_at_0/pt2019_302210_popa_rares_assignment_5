package parser;

import model.MonitoredData;
import org.joda.time.Duration;
import org.joda.time.LocalDate;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;

public class Parser {
    ArrayList<MonitoredData> data;

    public Parser() {
        data = new ArrayList<>();
    }

    public void parse() throws IOException {
        Stream<String> lines = Files.lines(Paths.get("Activities.txt"));
        lines
                .map(x -> x.split("\t\t"))
                .forEach(x -> data.add(new MonitoredData(x[0], x[1], x[2])));
        lines.close();
    }

    public int getDayCount() {
        return data.stream()
                .collect(groupingBy(d -> d.getStart().toLocalDate()))
                .size();
    }

    public Map<String, Long> getActivitiesCount() {
        return data.stream()
                .collect(groupingBy(MonitoredData::getActivity, Collectors.counting()));
    }

    public Map<LocalDate, Map<String, Long>> getActivitiesPerDay() {
        return data.stream()
                .collect(groupingBy(d -> d.getStart().toLocalDate(), groupingBy(MonitoredData::getActivity, Collectors.counting())));
    }

    public Map<MonitoredData, Duration> getDurationPerEntry() {
        return data.stream()
                .collect(toMap(Function.identity(), MonitoredData::calcDuration));
    }

    public Map<String, Duration> getDurationPerActivity() {
        return data.stream()
                .collect(toMap(MonitoredData::getActivity, MonitoredData::calcDuration, Duration::plus));
    }

    public List<String> getFilteredActivities() {
        return data.stream()
                .collect(toMap(MonitoredData::getActivity,
                         d -> d.calcDuration().getMillis() > 300000 ? new int[]{1, 0} : new int[]{1, 1},
                        (x, y) -> {
                            x[0] += y[0];
                            x[1] += y[1];
                            return x;
                        }))
                .entrySet().stream()
                .filter(d -> (double)d.getValue()[1] / d.getValue()[0] >= 0.9)
                .map(Map.Entry::getKey)
                .collect(toList());
    }

    public ArrayList getDataList() { return data; }
}
